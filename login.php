<?php
       session_start(); // Reset session
       $dsn = "mysql:dbname=C2209L;host=localhost;port=3306";
       $user = "C2209L";
       $pwd = "Thuthuy@182!";
       $_SESSION["user_email"] = "";

       if($_POST){
              $email = $_POST['email'];
              $password = $_POST['password'];
              try{
                     $conn = new PDO($dsn, $user, $pwd);
                     $sql = "SELECT email, password FROM tblUser WHERE email=? AND password=?";
                     $hashPwd = sha1($password);
                     $stmt = $conn->prepare($sql);
                     $stmt->bindParam(1, $email);
                     $stmt->bindParam(2, $hashPwd);

                     $stmt->execute();
                     $rs = $stmt->fetch(PDO::FETCH_ASSOC);
                     if(strlen($rs["email"])>0){
                            $_SESSION["user_email"] = $rs["email"];
                     } else {
                            echo "Invalid email or password";
                     }
                     if($_SESSION["user_email"] != ""){
                            header("Location: home.php");
                     }
              } catch (PDOException $e) {
                     echo "Connection failed: " . $e->getMessage();
              }
       }
?>

<!DOCTYPE html>

<html>
       <head>
              <meta charset="utf-8">
              <meta http-equiv="X-UA-Compatible" content="IE=edge">
              <title></title>
              <meta name="description" content="">
              <meta name="viewport" content="width=device-width, initial-scale=1">
              <link rel="stylesheet" href="">
       </head>
       <body>
              <form method="post">
                     <input type="text" name="email" id="email" placeholder="Username">
                     <input type="password" name="password" id="password" placeholder="Password">
                     <input type="submit" name="submit" value="Login" onclick="return login()">
              </form>
       </body>
</html>

<script type="text/javascript">
       function login() {
              let email = document.getElementById("email").value;
              let password = document.getElementById("password").value;
              let emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
              let passwordRegex = /^.{6,}$/;
              if(email==''||password=='') {
                     alert("Please enter your email and password");
                     return false;
              }
              if(!emailRegex.test(email)) {
                     alert("Please enter a valid email address");
                     return false;
              }
              if(!passwordRegex.test(password)) {
                     alert("Please enter a password that is at least 6 characters long");
                     return false;
              }
       }
</script>